package ru.sherb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Дано прямоугольное поле из 0 и 1, где 0 - вода, 1 - суша.
 * Две клетки соседние если они соединены по стороне.
 * Назовём острова - связные клетки окружённые водой или границей карты.
 * 1) Найти количество островов
 * 2) Найти количество островов разной формы
 * 3) запараллелить алгоритм
 */
public final class Islands {

    static final class Cell {

        public static Cell of(Integer x, Integer y) {
            return new Cell(x, y);
        }

        final Integer x;
        final Integer y;

        private Cell(Integer x, Integer y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Cell)) return false;
            Cell cell = (Cell) o;
            return Objects.equals(x, cell.x) && Objects.equals(y, cell.y);
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y);
        }
    }

    static class Island {

        public static Island getInstance(Cell border) {
            return new Island(border);
        }

        private final Set<Cell> border;
        private int deltaX;
        private int deltaY;

        private Island(Cell cell) {
            this.border = new HashSet<>();
            this.deltaX = cell.x;
            this.deltaY = cell.y;
            this.border.add(cell);
        }


        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Island other = (Island) o;
            if (this.border.size() != other.border.size()) return false;
            Set<Cell> thisBorder = getRelativeBorder(this.border, this.deltaX, this.deltaY);
            Set<Cell> otherBorder = getRelativeBorder(other.border, other.deltaX, other.deltaY);

            return thisBorder.containsAll(otherBorder);
        }

        @Override
        public int hashCode() {
            return Objects.hash(getRelativeBorder(border, deltaX, deltaY));
        }

        public boolean belongs(Cell cell) {
            return border.contains(cell);
        }

        public void addBorder(Cell cell) {
            if (cell.x < deltaX) {
                deltaX = cell.x;
            }
            if (cell.y < deltaY) {
                deltaY = cell.y;
            }
            border.add(cell);
        }

        public void merge(Island otherIsland) {
            for (Cell cell : otherIsland.border) {
                addBorder(cell);
            }
        }

        int borderSize() {
            return border.size();
        }

        private static Set<Cell> getRelativeBorder(Set<Cell> absoluteBorder, int deltaX, int deltaY) {
            return absoluteBorder.stream()
                    .map(cell -> Cell.of(cell.x - deltaX, cell.y - deltaY))
                    .collect(Collectors.toSet());
        }
    }

    // 1. получаем все пограничные клетки
    // 2. делим их количество по потокам
    // 3. в каждом потоке своя коллекция собранных островов
    // 4. после завершения работы всех потоков объединяем все найденные острова в коллекцию и мержим смежные
    static Set<Island> getAllIslandsParallel(int[][] field) {
        List<Island> result = new ArrayList<>();
        List<Callable<Set<Island>>> futures = new ArrayList<>();
        Set<Cell> borders = Stream.iterate(0, i -> i + 1).limit(field.length).parallel()
                .flatMap(i -> Stream.iterate(0, j -> j + 1).limit(field[i].length).parallel()
                        .filter(j -> field[i][j] == 1)
                        .filter(j -> i == 0
                                || i == field.length - 1
                                || j == 0
                                || j == field[i].length - 1
                                || isBorder(i, j, field))
                        .map(j -> Cell.of(i, j)))
                .collect(Collectors.toSet());


        List<Island> islands = new ArrayList<>();
        for (Cell border : borders) {
            List<Cell> neighbors = generateNeighbors(border);
            List<Island> containsIsland = new ArrayList<>();
            for (Island island : islands) {
                if (containsAny(island, neighbors)) {
                    containsIsland.add(island);
                }
            }

            if (!containsIsland.isEmpty()) {
                Island island = containsIsland.remove(containsIsland.size() - 1);
                island.addBorder(border);
                for (Island otherIsland : containsIsland) {
                    island.merge(otherIsland);
                }
                islands.removeAll(containsIsland);
            } else {
                Island island = Island.getInstance(border);
                islands.add(island);
            }
        }

        for (Island island : islands) {
            result.add(island);
            for (Island completeIsland : result) {
                if (containsAny(completeIsland, island.border)) {
                    // TODO: 15.04.2018 очень плохой алгоритм O(n^2)
                    // сделать с общей блокировкой на islands
                }
            }
        }

        return new HashSet<>(result);
    }


    static Set<Island> getAllIslands(int[][] field) {
        List<Island> islands = new ArrayList<>();
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (field[i][j] == 0) {
                    continue;
                }
                boolean isEndCell = i == 0
                        || i == field.length - 1
                        || j == 0
                        || j == field[i].length - 1;

                if (isEndCell || isBorder(i, j, field)) {
                    Cell current = Cell.of(i, j);
                    List<Cell> neighbors = generateNeighbors(current);
                    List<Island> containsIsland = new ArrayList<>();
                    for (Island island : islands) {
                        if (containsAny(island, neighbors)) {
                            containsIsland.add(island);
                        }
                    }

                    if (!containsIsland.isEmpty()) {
                        Island island = containsIsland.remove(containsIsland.size() - 1);
                        island.addBorder(current);
                        for (Island otherIsland : containsIsland) {
                            island.merge(otherIsland);
                        }
                        islands.removeAll(containsIsland);
                    } else {
                        Island island = Island.getInstance(current);
                        islands.add(island);
                    }
                }
            }
        }
        return new HashSet<>(islands);
    }

    private static boolean containsAny(Island island, Collection<Cell> neighbors) {
        return neighbors.stream().anyMatch(island::belongs);
    }

    private static boolean isBorder(int x, int y, int[][] field) {
        return field[x - 1][y] == 0
                || field[x + 1][y] == 0
                || field[x][y - 1] == 0
                || field[x][y + 1] == 0
                // workaround
                || field[x - 1][y - 1] == 0
                || field[x - 1][y + 1] == 0
                || field[x + 1][y - 1] == 0
                || field[x + 1][y + 1] == 0;
    }

    private static List<Cell> generateNeighbors(Cell cell) {
        List<Cell> result = new ArrayList<>();
        result.add(Cell.of(cell.x - 1, cell.y));
        result.add(Cell.of(cell.x + 1, cell.y));
        result.add(Cell.of(cell.x, cell.y - 1));
        result.add(Cell.of(cell.x, cell.y + 1));
        return result;
    }
}
