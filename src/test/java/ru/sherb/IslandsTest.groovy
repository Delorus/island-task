package ru.sherb

import static groovy.util.GroovyTestCase.assertEquals
import static ru.sherb.Islands.*

class IslandsTest extends GroovyTestCase {

    void 'test всего 23 граничащих клетки'() {
        int[][] field = [
                ['□', '□', '□', '□', '■', '■'],
                ['□', '■', '□', '□', '■', '□'],
                ['■', '■', '■', '□', '■', '■'],
                ['■', '■', '■', '■', '□', '□'],
                ['■', '■', '■', '■', '□', '■'],
                ['■', '■', '■', '■', '□', '□'],
                ['□', '□', '□', '□', '■', '■'],
                ['□', '□', '□', '□', '■', '■']
        ].collectNested { el ->
            el == '□' ? 0 : 1
        }
        def expected = 23

        def actual = getAllIslands(field).sum { (it as Island).borderSize() }

        assertEquals(expected, actual)
    }

    void 'test всего 4 острова'() {
        int[][] borders = [
                //0    1     2    3     4    5
                ['□', '□', '□', '□', '■', '■'], // 0
                ['□', '■', '□', '□', '■', '□'], // 1
                ['□', '■', '■', '□', '■', '■'], // 2
                ['■', '■', '■', '□', '□', '□'], // 3
                ['□', '□', '□', '■', '■', '□'], // 4
                ['■', '□', '□', '■', '■', '□'], // 5
        ].collectNested { el ->
            el == '□' ? 0 : 1
        }
        def expected = 4

        def actual = getAllIslands(borders).size()

        assertEquals(expected, actual)
    }

    void 'test всего 3 острова разной формы'() {
        int[][] borders = [
                //0    1     2    3     4    5
                ['□', '■', '□', '□', '■', '■'], // 0
                ['□', '□', '□', '□', '■', '□'], // 1
                ['□', '■', '■', '□', '■', '■'], // 2
                ['□', '■', '■', '□', '□', '□'], // 3
                ['□', '□', '□', '■', '■', '□'], // 4
                ['■', '□', '□', '■', '■', '□'], // 5
        ].collectNested { el ->
            el == '□' ? 0 : 1
        }
        def expected = 3

        def actual = getAllIslands(borders).size()

        assertEquals(expected, actual)
    }

    void 'test equals and hashCode'() {
        Cell cell = Cell.of(2, 3)
        Cell cell2 = Cell.of(3, 4)
        Cell cell3 = Cell.of(4, 5)
        Island first = Island.getInstance(cell)
        Island second = Island.getInstance(cell2)
        Island third = Island.getInstance(cell3)
        Island nonEquals = Island.getInstance(cell2); nonEquals.addBorder(cell)

        assertTrue(first == second)
        assertFalse(first == nonEquals)

        // reflective
        assertTrue(first == first)

        // symmetric
        assertTrue(first == second)
        assertTrue(second == first)
        assertFalse(first == nonEquals)
        assertFalse(nonEquals == first)

        // transitive
        assertTrue(first == second && second == third && first == third)

        assertTrue(first == second && first.hashCode() == second.hashCode())

        first.addBorder(cell2)
        assertTrue(first == nonEquals)
        assertFalse(first == second)
    }
}
